package dictionary;

import java.util.Comparator;
/** Lớp Data quản lí 1 từ tiếng anh và nghĩa của nó 
 * @since 2018 10 10
 * @author Hoàng Sơn Tùng 17021351
 * @version 1.0
 */
public class Data {
    // Khai báo thuộc tính: text là từ tiếng anh, dec là nghĩa là nghĩa tiếng việt
    private String text, dec;
    
    // constructor
    public Data(String text, String dec) {
        this.text = text;
        this.dec = dec;
}
    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the dec
     */
    public String getDec() {
        return dec;
    }

    /**
     * @param dec the dec to set
     */
    public void setDec(String dec) {
        this.dec = dec;
    }
    
    /** Phương thức tạo 1 comparator đê so sánh 2 Data về thuộc tính text
     * @param tham số thuộc kiểu Data
     * @return giá trị boolean so sánh 2 Data về mặt thuộc tính text
     */
    public static Comparator<Data> textComparator = new Comparator<Data>(){
        public int compare(Data d1, Data d2){
           String text1 = d1.getText().toUpperCase();
	   String text2 = d2.getText().toUpperCase();
	   return text1.compareTo(text2);
        }
    };
}
